library model;

class User {
  final String name;
  final String firstName;
  final String function;
  final String image;
  final String helloMessage;


  User({this.name, this.firstName, this.function, this.image, this.helloMessage});

  factory User.fromJson(Map<String, dynamic> json){
    return User(
      name: json['name'],
      firstName: json["firstname"],
      function: json['function'],
      image: json["image"],
      helloMessage: json['helloMessage'],
    );
  }
}
