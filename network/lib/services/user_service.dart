import 'dart:convert';

import 'package:model/user.dart';

import '../repository/user_repository.dart';


abstract class UserService {
  Future<User> fetchUsers();
}

class UserServiceImpl implements UserService {
  final UserRepository _userRepository;

  UserServiceImpl(this._userRepository);

  @override
  Future<User> fetchUsers() async {
    var response = await _userRepository.fetchUsers();
    return User.fromJson(json.decode(response.body));
  }
}
