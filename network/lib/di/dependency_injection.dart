import '../repository/user_repository.dart';
import '../services/user_service.dart';

abstract class DependencyContainer{
  UserService userService();
}


class Injector implements DependencyContainer {
  UserService _userService;

  Injector();

  @override
  UserService userService() {
    if (_userService == null) {
      _userService = new UserServiceImpl(new UserRepositoryImpl());
    }
    return _userService;
  }

  Injector._internal();
}
