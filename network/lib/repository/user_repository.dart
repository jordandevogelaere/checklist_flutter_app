import 'package:model/user.dart';

import 'package:http/http.dart' as http;
import 'package:network/utils/http_utils.dart';

abstract class UserRepository {
  Future<http.Response> fetchUsers();
}

class UserRepositoryImpl implements UserRepository {
  @override
  Future<http.Response> fetchUsers() async {
    String _endpoint = "publicinfo/hr";
    var url = HttpUtils.baseUrl + _endpoint;
    return http.get(url);
  }
}
