# Checklist application

TBD

## Flutter installation
 -  [Download Flutter SDK](https://storage.googleapis.com/flutter_infra/releases/stable/macos/flutter_macos_v1.0.0-stable.zip)
 - Extract .zip and locate the directory
 - Add Flutter SDK to your .bash-profile (export PATH="$PATH:`pwd`/flutter/bin")
 - Run: `flutter doctor` and follow the instructions

## Project structure
- lib: Here you can find all widgets of the application
- network: Here you need to make new repositories and services using the client.dart (/utils)
- model: Here you need to define your models
