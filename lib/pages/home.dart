import 'package:flutter/material.dart';
import 'package:model/user.dart';
import 'package:network/di/dependency_injection.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:io';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: [
    'email',
  ],
);

class HomePage extends StatefulWidget {
  final DependencyContainer _dependencyContainer;

  HomePage(this._dependencyContainer);

  @override
  State<StatefulWidget> createState() {
    return new _NameWidget(_dependencyContainer);
  }
}

class _NameWidget extends State<HomePage> {
  final DependencyContainer _dependencyContainer;
  String _name = "";
  String _function = "";
  String _helloMessage = "";
  String _image = "";
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  _NameWidget(this._dependencyContainer);

  @override
  void initState() {
    user().then((val) => setState(() {
          _name = "${val.firstName} ${val.name}";
          _function = "${val.function}";
          _helloMessage = "${val.helloMessage}";
          _image = "${val.image}";
        }));
    _firebaseCloudMessagingListeners();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(_name),
          ),
          Center(
            child: Text(_function),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: Text(
                _helloMessage,
                textAlign: TextAlign.center,
              ),
            ),
          ),
          FlatButton(
            child: Text("Google Sign in"),
            onPressed: () => _handleSignIn(),
          ),
          FlatButton(
            child: Text("Sign out"),
            onPressed: () => _handleSignOut(),
          ),
        ],
      ),
    );
  }

  Future<User> user() async {
    return await _dependencyContainer.userService().fetchUsers();
  }

  Future<void> _handleSignIn() async {
    try {
      var response = await _googleSignIn.signIn();
      print(response.email);
    } catch (error) {
      print(error);
    }
  }

  Future<void> _handleSignOut() async {
    _googleSignIn.disconnect();
    print("LOGGED OUT");
  }

  void _firebaseCloudMessagingListeners() {
    if (Platform.isIOS) _iOSPermission();

    _firebaseMessaging.getToken().then((token) {
      print("TOKEN $token");
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        var test = message["aps"];
        Map<String, dynamic> lol = test["alert"];
        print(lol);
       // print(message["aps"]["alert"]);
       // PushNotification aps = new PushNotification(message["aps"]);
       // Alert alert = new Alert(aps.alert.title, aps.alert.body);

        showDialog(
            context: context,
            builder: (_) => new AlertDialog(
                  title: new Text(""),
                  content: new Text(""),
                ));
      },
      onResume: (Map<String, dynamic> message) async {
        var test = message["aps"];
        Map<String, dynamic> lol = test["alert"];
        print(lol);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print(message);
      },
    );
  }

  void _iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }
}

class PushNotification {
  Alert alert;

  PushNotification(this.alert);
}

class Alert {
  String title;
  String body;

  Alert(this.title, this.body);
}
