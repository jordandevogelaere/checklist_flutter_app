import 'package:checklist_app/pages/home.dart';
import 'package:flutter/material.dart';
import 'package:network/di/dependency_injection.dart';

void main() {
  runApp(MyApp(Injector()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final DependencyContainer _dependencyContainer;

  MyApp(this._dependencyContainer);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Checklist app"),
        ),
        body: HomePage(_dependencyContainer),
      ),
    );
  }
}
